<?php

        function apritisesamo ($istanza,$idogg,$cr,$ser) {
         $ordor= array();
         $t=1;
          if (strcasecmp($cr,'decrescente')==0) $t=-1;
          //Per caricare n file, portare $i ad n+1
          for ($i=1;$i<11;$i++){
            if(($istanza['ord_allegato'.$i]=='')||($istanza['desc_allegato'.$i]=='')){
              continue;
            }
            $ordor['file'.$i] = array (
              "ord" => $istanza['ord_allegato'.$i],
              "descr" => $istanza['desc_allegato'.$i],
              "all" => $istanza['allegato'.$i],
              "cr" => $t);
          }
		  if (empty($ordor)) {
			  return 0;
		  }
          //Funzione per testare se l'indice è presente e diverso da 0
          function tesnum($l){
            if (!(is_numeric($l))||$l==0){
              return FALSE;
            }
            else {
              return TRUE;
              }
          }
          usort($ordor, function($a, $b) {
            //Se sono presenti indici di ordinamento
            if(tesnum($a['ord'])&&tesnum($b['ord'])){
              return $a['cr']*($a['ord'] - $b['ord']);
            }
            //Se è presente un solo indice di ordinamento
            else if(tesnum($a['ord']) ==! tesnum($b['ord'])){
              // Se è a ad avere un numero ordinamento, mettilo prima
              if (tesnum($a['ord'])){
                return -1;
              }
              else {
                return 1;
              }
            }
            //Se non ci sono indici di oridnamento
            else {
              return strcasecmp($a['descr'],$b['descr']);
              }
            }
          );
          //Hordor!
          function dime($url) {
                  //Fallimento default
                  $result=-1;
                  $curl=curl_init($url);
                  //Non prendere il body
                  curl_setopt( $curl, CURLOPT_NOBODY, true );
                  //Includi header in output
                  curl_setopt( $curl, CURLOPT_HEADER, true );
                  //Non stampare, fai return di una stringa
                  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
                  //Segui eventuali redirect
                  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
                  //Esegui la sessione di client URL
                  $data = curl_exec( $curl );
                  //Chiusura clientUrl
                  curl_close( $curl );

                  if( $data ) {
                    //Fallimento default
                    $content_length = "unknown";
                    $status = "unknown";
                    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
                      $status = (int)$matches[1];
                    }
                    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
                      $content_length = (int)$matches[1];
                    }
                    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
                    if( $status == 200 || ($status > 300 && $status <= 308) ) {
                      //Risultato in kB
                      $result = $content_length/1024;
                      //Risultato in MB se necessario
                      if ($result>1024){
                        $result /= 1024;
                        $result = round($result,2);
                        $result .= ' MB)';
                      }
                      else{
                        $result = round($result,0);
                        $result .= ' kB)';
                        }
                    }
                  }
                  return $result;
              }
          //Indirizzo server, il loro non funziona più per ragioni non pervenute.
          //$ser='http://srv-isweb.comune.prato.it/trasparenzaprato';
          //Non ha bisogno di alcuna modifica anche variando il numero di files
          $agh=0;
          foreach($ordor as $file){
            if ($file['all']=='') {return 0;}
            else if ($agh==0){
            	echo "<ul>";
            	$agh=1;
            }
 	  //  echo $file['ord'];         
            $res='/moduli/downloadFile.php?file='.datoOgg($idogg, 'tabella').'/'.$file['all'];
            //Determinazione estensione
            $ext = substr(strrchr($ser.$res, "."), 1);
            $es=array('doc','docx','jpg','ods','odt','pdf','ppt','rtf','xls','xlsx','zip');
            //Formazione link
            echo '<li class="listanascosta"><img class="imgcentro" src="'.$ser.'/grafica/file/small/';
            foreach ($es as $e){
              if(strcasecmp($ext,$e)) {
                echo $ext.'.gif" alt="File '.$ext.'"/> ';
                break;
              }
              else {echo 'generica.gif alt="File '.$ext.'"/>'; }
            }
            echo '<a href="' .$ser. $res . '">' . $file['descr'] . '</a> (' . dime($ser.$res).'</li>';
          }
          echo "</ul>";
        }
        
    ?> 